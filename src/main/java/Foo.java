import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

/**
 * Created by Paul on 13/05/2015.
 */
public class Foo {
    static final Logger logger = LoggerFactory.getLogger(Foo.class);

    public Foo(String s) {
        MDC.put("AID", s);
    }

    public void doIt() {
        logger.debug("Did it again!");
        logger.info("Did it again! in info");
        logger.debug("Did it again! in debug");
        logger.warn("Did it again! in warn");
    }
}
