import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.turbo.DynamicThresholdFilter;
import ch.qos.logback.classic.turbo.MDCValueLevelPair;
import ch.qos.logback.core.spi.FilterReply;
import ch.qos.logback.core.util.StatusPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

/**
 * Created by Paul on 13/05/2015.
 */
public class MyLogger {
    final static Logger logger = LoggerFactory.getLogger(MyLogger.class);

    public static void main(String[] args) {
        // assume SLF4J is bound to logback in the current environment
        LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
       logger.info("Turbos=" + lc.getTurboFilterList().size());
        // print logback's internal status
        //StatusPrinter.print(lc);
        logger.info("Entering application.");
        logger.debug("wibbly");
        Foo foo = new Foo("12345");
        foo.doIt();
        Foo foo2 = new Foo("9999999");
        foo2.doIt();
        DynamicThresholdFilter filter =new DynamicThresholdFilter();
        filter.setDefaultThreshold(lc.getLogger(ch.qos.logback.classic.Logger.ROOT_LOGGER_NAME).getLevel());
        filter.setKey("AID");
        filter.setOnHigherOrEqual(FilterReply.ACCEPT);
        filter.setOnLower(FilterReply.DENY);
        MDCValueLevelPair vp=new MDCValueLevelPair();
        vp.setLevel(Level.DEBUG);
        vp.setValue("12345");
        filter.addMDCValueLevelPair(vp);
        lc.addTurboFilter(filter);
        filter.start();
        //lc.start();
        logger.info("Turbos="+lc.getTurboFilterList().size());
        Foo foo3 = new Foo("12345");
        foo3.doIt();
        Foo foo4 = new Foo("9999999");
        foo4.doIt();
        MDC.clear();
        lc.resetTurboFilterList();
        Foo foo5 = new Foo("12345");
        foo5.doIt();
        Foo foo6 = new Foo("9999999");
        foo6.doIt();
        MDC.clear();
        logger.info("Exiting application.");
    }

}
